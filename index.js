import express from 'express';
import { router as todoRouter } from './backend/todo/todo.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect('mongodb://localhost/teamtodolist');

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/todos', todoRouter);

mongoose.connection.once('open', () => {
  console.log('Connected to MongoDB');
  app.listen(3001, () => {
    console.log('Server listens to http://localhost:3001');
  });
});
