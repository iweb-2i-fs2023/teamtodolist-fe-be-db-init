import { model as Todo } from './todo.model.js';

async function getTodos(request, response) {
  await Todo.find()
    .exec()
    .then((todos) => {
      response.json(todos);
    });
}

async function createTodo(request, response) {
  await Todo.create({
    description: request.body.description,
    isDone: request.body.isDone,
  }).then((todo) => {
    response.json(todo);
  });
}

async function updateTodo(request, response) {
  const todoId = request.params.id;
  await Todo.findById(todoId)
    .exec()
    .then((todo) => {
      if (!todo) {
        return Promise.reject({
          message: `TODO with id ${todoId} not found.`,
          status: 404,
        });
      }
      todo.description = request.body.description;
      todo.isDone = request.body.isDone;
      return todo.save();
    })
    .then((savedTodo) => {
      response.json(savedTodo);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

async function removeTodo(request, response) {
  const todoId = request.params.id;
  await Todo.findById(todoId)
    .exec()
    .then((todo) => {
      if (!todo) {
        return Promise.reject({
          message: `TODO with id ${todoId} not found.`,
          status: 404,
        });
      }
      return todo.deleteOne();
    })
    .then((deletedTodo) => {
      response.json(deletedTodo);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getTodos, createTodo, updateTodo, removeTodo };
