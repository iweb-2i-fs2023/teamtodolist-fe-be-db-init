import { todosService, membersService } from './services.js';
import {
  createTodoList,
  createMemberList,
  LIST_EVENT,
} from './listComponent.js';
import {
  getTodoInput,
  getMemberInput,
  updateMemberDropdown,
} from './inputComponent.js';

async function addNewListItem(getNewItem, service, items, createItemList) {
  const item = getNewItem();
  if (item) {
    // Neues Item in Backend speichern
    await service.create(item).then((newItem) => {
      // Neues Item Frontend hinzufügen
      items.push(newItem);
      createItemList();
    });
  }
}

async function addNewTodo() {
  await addNewListItem(getTodoInput, todosService, todos, recreateTodoList);
  recreateMemberList();
}

async function addNewMember() {
  await addNewListItem(
    getMemberInput,
    membersService,
    members,
    recreateMemberList
  );
  updateMemberDropdown(members);
}

function recreateTodoList() {
  function handleTodoListEvent(eventType, todoId, requestData) {
    switch (eventType) {
      case LIST_EVENT.UPDATE:
        todosService.update(todoId, requestData).then((updatedTodo) => {
          // Status erledigt / nicht erledigt in Frontend updaten
          const toChange = todos.find((todo) => todo._id === todoId);
          toChange.isDone = updatedTodo.isDone;
          // Todo Liste in DOM neu erzeugen
          recreateTodoList();
        });
        break;
      case LIST_EVENT.DELETE:
        // Todo im Backend löschen
        todosService.remove(todoId).then(() => {
          // Todo im Frontend löschen
          const idxToDelete = todos.findIndex((todo) => todo._id === todoId);
          todos.splice(idxToDelete, 1);
          // Todo Liste in DOM neu erzeugen
          recreateTodoList();
          // Teammitglieder Liste muss auch angepasst werden
          recreateMemberList();
        });
        break;
    }
  }

  createTodoList(todos, members, handleTodoListEvent);
}

function recreateMemberList() {
  function handleMemberListEvent(eventType, memberId, requestData) {
    if (eventType === LIST_EVENT.DELETE) {
      // Teammitglieder im Backend löschen
      membersService.remove(memberId).then(() => {
        // Teammitglieder im Frontend löschen
        const idxToDelete = members.findIndex((resp) => resp._id === memberId);
        members.splice(idxToDelete, 1);
        // Personen Liste in DOM neu erzeugen
        recreateMemberList();
        updateMemberDropdown(members);
      });
    }
  }

  createMemberList(members, todos, handleMemberListEvent);
}

const elAddTodoBtn = document.getElementById('add-new-todo');
elAddTodoBtn.addEventListener('click', addNewTodo);
const elAddRespBtn = document.getElementById('add-new-member');
elAddRespBtn.addEventListener('click', addNewMember);

let todos = [];
let members = [];
// Zu Beginn alle Todos und Teammitglieder vom Backend laden
await membersService.getAll().then((data) => {
  // Geladene Teammitglieder in Frontend in Array speichern
  for (let i = 0; i < data.length; i++) {
    const member = data[i];
    members.push(member);
  }
});
await todosService.getAll().then((data) => {
  // Geladene Todos in Frontend in Array speichern
  for (let i = 0; i < data.length; i++) {
    const todo = data[i];
    todos.push(todo);
  }
});
// Teammitglieder Dropdown in Eingabebereich hinzufügen
updateMemberDropdown(members);
// Listen mit Todos und Teammitgliedern erstellen
recreateTodoList();
recreateMemberList();
